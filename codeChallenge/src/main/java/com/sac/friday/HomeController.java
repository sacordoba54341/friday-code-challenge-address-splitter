package com.sac.friday;



import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/** Simple controller class for home page
 * 
 * @author 	Sebastian Cordoba
 * @version	1.0
 */

@Controller
public class HomeController {

	    @GetMapping("/")
	    public String homePage(Model model) {
	        return "home";
	    }

	    @PostMapping("/")
	    public String splitAddressLine(@RequestParam(value="addressLineInput") String addressLine, Model model) {
        
	    	AddressSplitter addrssSpltr = new AddressSplitter();
	    	
	    	JSONObject JSONAddress = addrssSpltr.split(addressLine);

	    	model.addAttribute("JSONAddress", JSONAddress.toString());
	     
	    	return "home";
	    }

}