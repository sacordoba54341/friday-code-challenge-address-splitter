package com.sac.friday;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/** Simple error handler class
 * 
 * @author	Sebastian Cordoba
 * @version	1.0
 */

@Controller
public class CustomErrorController implements ErrorController {

	@RequestMapping("/error")
	public ModelAndView handleAllExceptions(Exception ex, HttpServletRequest request) {
	
		String stacktrace = ExceptionUtils.getStackTrace(ex);
		
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
	    Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");

	    stacktrace = "Status Code: " + statusCode  + (exception == null ? "": ", " + exception.getMessage()) + System.lineSeparator() + stacktrace;
		
	    ModelAndView modelAndView = new ModelAndView("error");
	    modelAndView.addObject("stack", stacktrace);
	    return modelAndView;
	    
	}


  @Override
  public String getErrorPath() {
      return "/error";
  }
}