package com.sac.friday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** Provides a simple functional web application for user testing on AddressSplitter class
 * 
 * @author 	Sebastian Cordoba
 * @version	1.0
 */
@SpringBootApplication
public class CodeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeChallengeApplication.class, args);
	}

}
