package com.sac.friday;

import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

/** Provides a method for splitting an address string into a JSON object, with fields "housenumber" and "street". Address must not include country name, nor state nor zip code.  
 * 
 * @author Sebastian Cordoba
 * @version 1.0
 * 
 */
public class AddressSplitter {
	
	/** Splits an address string into JSON object with house number and street name fields
	 * 
	 * @param	addressLine Address string to process
	 * @return	JSON object with fields "housenumber" and "street". Fields values will return empty if addressLine could not be split
	 * @throws	
	 */
	public JSONObject split(String addressLine) {

		String street = "";
		String houseNo = "";
		
	    if (addressLine != null) {

	    	// List of valid house number prefixes
	      	// More to be added to this list as needed... 
	    	List<String> houseNumberPrefixes = Arrays.asList("n", "no", "nº");
	    	
	      	// Assuming comma is used to separate house number from street name into 2 distinctive parts...
	    	// Space is default splitter...
	    	// More to be added to this list as needed... 
	      	List<String> stringSplitters = Arrays.asList(",", " ");
	      	String stringSplitter = ""; 
	      	
	      	for (String separator : stringSplitters) {
	      		if (addressLine.indexOf(separator) > 0) {
	      			stringSplitter = separator;
	      			break;
	      		}
	      	}
	      	
	      	if (!stringSplitter.isEmpty()) {
		      	String[] addressLineSplitted = addressLine.split(stringSplitter);

		      	String addressPart = "";
	        	int potencialHouseNumberStartIndex = -1;
	        	int potencialHouseNumberEndIndex = -1;
	        	
	        	// Numbers at the beginning of the string... 
	        	if (addressLineSplitted[0].trim().substring(0,1).matches("\\d")) {
	        		potencialHouseNumberStartIndex = 0;
	
	                if (addressLineSplitted[1].trim().length() == 1)
	                  potencialHouseNumberEndIndex = 1;
	                else
	                  potencialHouseNumberEndIndex = 0;

	        	// Other cases...
	        	} else {
	        		potencialHouseNumberEndIndex = addressLineSplitted.length - 1;

	        		for (int i=0; i < addressLineSplitted.length; i++) {
	        			if (addressLineSplitted[i].trim().substring(0, 1).matches("\\d"))
	        				potencialHouseNumberStartIndex = i;
		              }
	        	}	            	
	
	        	
	        	if (potencialHouseNumberStartIndex != -1) {
	        		
	        		if (potencialHouseNumberStartIndex != 0 && houseNumberPrefixes.contains(addressLineSplitted[potencialHouseNumberStartIndex - 1].trim().toLowerCase()))
	        			--potencialHouseNumberStartIndex;
	        		
	        			
	        		for (int i=0; i < addressLineSplitted.length; i++) {
	        			addressPart = addressLineSplitted[i].trim();
	
	        			if (potencialHouseNumberStartIndex <= i && potencialHouseNumberEndIndex >= i)
	        				houseNo += addressPart + " ";
	        			else
	        				street += addressPart + " ";
		              }
	
	        		houseNo = houseNo.trim();
	        		street = street.trim();

	        	} 
	        	
	        }
	      	
	    }
       
	    return new JSONObject("{\"street\": \"" + street + "\", \"housenumber\": \"" + houseNo + "\"}");
    
  }

	
}
