package com.sac.friday;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import java.util.stream.Stream;

@SpringBootTest
class CodeChallengeApplicationTests {
	
	private AddressSplitter addressSplitter;
    
	public static Stream<Arguments> inputAndExpected() {
		return Stream.of(
			Arguments.of("Winterallee 3", "{\"street\": \"Winterallee\", \"housenumber\": \"3\"}"),
			Arguments.of("Musterstrasse 45", "{\"street\": \"Musterstrasse\", \"housenumber\": \"45\"}"),
			Arguments.of("Blaufeldweg 123B", "{\"street\": \"Blaufeldweg\", \"housenumber\": \"123B\"}"),
			Arguments.of("Am Bächle 23", "{\"street\": \"Am Bächle\", \"housenumber\": \"23\"}"),
			Arguments.of("Auf der Vogelwiese 23 b", "{\"street\": \"Auf der Vogelwiese\", \"housenumber\": \"23 b\"}"),
			Arguments.of("4, rue de la revolution", "{\"street\": \"rue de la revolution\", \"housenumber\": \"4\"}"),
			Arguments.of("200 Broadway Av", "{\"street\": \"Broadway Av\", \"housenumber\": \"200\"}"),
			Arguments.of("Calle Aduana, 29", "{\"street\": \"Calle Aduana\", \"housenumber\": \"29\"}"),
			Arguments.of("Calle 39 No 1540", "{\"street\": \"Calle 39\", \"housenumber\": \"No 1540\"}"),
			Arguments.of("39 Calle No 1540", "{\"street\": \"Calle No 1540\", \"housenumber\": \"39\"}"),
			Arguments.of("San Luis 3375 planta alta", "{\"street\": \"San Luis\", \"housenumber\": \"3375 planta alta\"}")
			);
	}
	

	@BeforeEach
	private void setUp() {
		this.addressSplitter = new AddressSplitter();
	}

	@ParameterizedTest
	@MethodSource("inputAndExpected")
	public void testAddressSplitterTest(String input, String expected) throws JSONException {
		addressSplitter =  new AddressSplitter();
		
		System.out.println("Input: "  + input + " ; ExpectedResult: " + expected);
		
		assertEquals(new JSONObject(expected).toString(), this.addressSplitter.split(input).toString() );
	}

	
}
